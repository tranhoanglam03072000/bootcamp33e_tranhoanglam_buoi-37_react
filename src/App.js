import logo from "./logo.svg";
import "./App.css";
import LayoutFromQuanLy from "./FromQuanLy/LayoutFromQuanLy";

function App() {
  return (
    <div className="App">
      <LayoutFromQuanLy />
    </div>
  );
}

export default App;
