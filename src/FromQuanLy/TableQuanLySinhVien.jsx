import React, { Component } from "react";
import { connect } from "react-redux";
import {
  handleDeleteUser,
  handleEditUserAction,
} from "../redux/action/FromQuanLyAction";

class TableQuanLySinhVien extends Component {
  renderThongTinDanhSachQuanLy = () => {
    return this.props.danhSachQuanLy.map((thongTin, index) => {
      return (
        <tr key={index} height={70} style={{ verticalAlign: "middle" }}>
          <td>{index + 1}</td>
          <td>{thongTin.taiKhoan}</td>
          <td>{thongTin.hoTen}</td>
          <td>{thongTin.matKhau}</td>
          <td>{thongTin.email}</td>
          <td>{thongTin.soDienThoai}</td>
          <td>{thongTin.loaiNguoiDung == 1 ? "User" : "khách hàng"}</td>
          <td>
            <button
              onClick={() => {
                this.props.dispatch(handleDeleteUser(thongTin.taiKhoan));
              }}
              className="btn  btn-danger"
            >
              Xoá
            </button>
            <button
              onClick={() => {
                this.props.dispatch(handleEditUserAction(thongTin));
              }}
              className="btn  btn-primary"
            >
              chỉnh sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="py-5">
        <h1 className="text-white bg-dark">Danh sách người dùng</h1>
        <table className="table  table-bordered">
          <thead>
            <tr>
              <th>STT</th>
              <th>Tài Khoản</th>
              <th>Họ Tên</th>
              <th>Mật Khẩu</th>
              <th>Email</th>
              <th>Số điện thoại</th>
              <th>Loại người dùng</th>
              <th>Tuỳ Chọn</th>
            </tr>
          </thead>
          <tbody>{this.renderThongTinDanhSachQuanLy()}</tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    danhSachQuanLy: state.FromQuanLyReducer.danhSachQuanLy,
  };
};
export default connect(mapStateToProps)(TableQuanLySinhVien);
