import React, { Component } from "react";
import { connect } from "react-redux";
import {
  changeValueAction,
  handleSubmitAction,
  handleUpdateUserAction,
} from "../redux/action/FromQuanLyAction";

class FromDangKi extends Component {
  handleChangeValue = (e) => {
    let { value, type, name } = e.target;
    this.props.dispatchChangeValue(name, value);
  };
  render() {
    return (
      <div className="mt-5 border">
        <h1 className="text-white bg-primary">From đăng kí</h1>
        <form className="row px-3 pb-5 mt-5">
          <div className="col-5">
            {/* tài khoản */}
            <div className="input-group">
              <label className="input-group-text">Tài Khoản</label>
              <input
                value={this.props.value.taiKhoan}
                onChange={(e) => this.handleChangeValue(e)}
                name="taiKhoan"
                type="text"
                className="form-control"
              />
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.taiKhoan}
            </div>
            {/* mật khẩu  */}
            <div className="input-group mt-3">
              <label className="input-group-text">Mật khẩu</label>
              <input
                value={this.props.value.matKhau}
                onChange={(e) => this.handleChangeValue(e)}
                name="matKhau"
                type="password"
                className="form-control"
              />
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.matKhau}
            </div>
            {/* email  */}
            <div className="input-group mt-3">
              <label className="input-group-text">Email</label>
              <input
                value={this.props.value.email}
                onChange={(e) => this.handleChangeValue(e)}
                name="email"
                type="email"
                className="form-control"
              />
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.email}
            </div>
          </div>
          <div className="col-2"></div>
          <div className="col-5">
            {/* Họ tên  */}
            <div className="input-group ">
              <label className="input-group-text">Họ tên</label>
              <input
                value={this.props.value.hoTen}
                onChange={(e) => this.handleChangeValue(e)}
                name="hoTen"
                type="text"
                className="form-control"
              />
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.hoTen}
            </div>
            {/* số điện thoại */}
            <div className="input-group mt-3">
              <label className="input-group-text">Số điện thoại</label>
              <input
                value={this.props.value.soDienThoai}
                onChange={(e) => this.handleChangeValue(e)}
                name="soDienThoai"
                type="text"
                className="form-control"
              />
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.soDienThoai}
            </div>
            {/* loại người dùng  */}
            <div className="input-group mt-3">
              <label className="input-group-text">Loại người dùng</label>
              <select
                onChange={(e) => {
                  this.handleChangeValue(e);
                }}
                value={this.props.value.loaiNguoiDung}
                name="loaiNguoiDung"
                className="form-select"
              >
                <option value="">Chọn loại người dùng</option>
                <option value={1}>User</option>
                <option value={2}>Khách hàng</option>
              </select>
            </div>
            <div
              style={{ height: "25px" }}
              className="text-danger mb-5 text-left "
            >
              {this.props.error.loaiNguoiDung}
            </div>
          </div>

          {/* // SUBMIT  */}
          <button
            type="submit"
            className="btn btn-success offset-3 col-2"
            onClick={(event) => {
              event.preventDefault();
              this.props.handleRegisterFrom();
            }}
          >
            Đăng ký
          </button>
          <button
            type="button"
            onClick={() => this.props.handleUpdateUser(this.props.value)}
            className="btn btn-primary offset-2 col-2"
          >
            Cập nhật
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    value: state.FromQuanLyReducer.value,
    error: state.FromQuanLyReducer.error,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    dispatchChangeValue: (name, value) => {
      dispatch(changeValueAction(name, value));
    },
    handleRegisterFrom: () => {
      dispatch(handleSubmitAction());
    },
    handleUpdateUser: (user) => {
      dispatch(handleUpdateUserAction(user));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FromDangKi);
