import React, { Component } from "react";
import FromDangKi from "./FromDangKi";
import TableQuanLySinhVien from "./TableQuanLySinhVien";

export default class LayoutFromQuanLy extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <FromDangKi />
        </div>
        <TableQuanLySinhVien />
      </div>
    );
  }
}
