import {
  CHANGE_VALUE,
  DELETE_USER,
  EDIT_USER,
  SUB_MIT,
  UPDATE_USER,
} from "../constant/FromQuanLyConstant";
import Swal from "sweetalert2";
const initialState = {
  value: {
    taiKhoan: "",
    matKhau: "",
    email: "",
    hoTen: "",
    soDienThoai: "",
    loaiNguoiDung: "",
  },
  error: {
    taiKhoan: "",
    matKhau: "",
    email: "",
    hoTen: "",
    soDienThoai: "",
    loaiNguoiDung: "",
  },
  danhSachQuanLy: [],
};

export const FromQuanLyReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_VALUE:
      {
        let newValue = { ...state.value };
        newValue = { ...newValue, [payload.name]: payload.value };
        state.value = newValue;
        // Validation Tổng quát Model Regex
        let testRegex = (regex, message) => {
          if (regex.test(payload.value)) {
            state.error = {
              ...state.error,
              [payload.name]: "",
            };
          } else {
            state.error = {
              ...state.error,
              [payload.name]: `*${message} `,
            };
          }
        };
        // ----------------------Validate ----------------
        // validation khoảng trắng
        if (payload.value == "") {
          state.error = {
            ...state.error,
            [payload.name]: `* ${payload.name} bạn chưa điền  `,
          };
        } else {
          state.error = {
            ...state.error,
            [payload.name]: "",
          };
        }
        //    ------------------------------------------- // xét các payloadvalue.length > 1
        if (payload.value.length >= 1) {
          switch (payload.name) {
            //validation tài Khoản
            case "taiKhoan":
              {
                testRegex(
                  /^[a-zA-Z0-9_]*$/,
                  "không được chứa khoảng trắng và ký tự đặc biệt"
                );
              }
              break;
            // Validation Mật khẩu
            case "matKhau":
              {
                testRegex(
                  /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                  "Tối thiểu tám ký tự, ít nhất một chữ cái và một số, không chứa kí tự đặc biệt"
                );
              }
              break;
            //validation email
            case "email":
              {
                testRegex(
                  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                  "chưa đúng định dạng"
                );
              }
              break;
            //validation họ tên
            case "hoTen":
              {
                testRegex(
                  /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
                  "phải là chữ"
                );
              }
              break;
            //validation Số điện thoại
            case "soDienThoai":
              {
                testRegex(
                  /(84|0[3|5|7|8|9])+([0-9]{8})+$\b/,
                  "vui lòng nhập đúng số điện thoại"
                );
              }
              break;
            //Loại người dùng
            case "loaiNguoiDung": {
              if (payload.value == "") {
                state.error = {
                  ...state.error,
                  [payload.name]: "Bạn chưa chọn loại người dùng",
                };
              } else {
                state.error = {
                  ...state.error,
                  [payload.name]: "",
                };
              }
            }
          }
        }
        return { ...state };
      }
      break;
    case SUB_MIT:
      {
        //validation submit
        let isValid = true;
        let { value, error } = state;
        //kiểm tra trung tài khoản
        let index = state.danhSachQuanLy.findIndex(
          (thongtin) => thongtin.taiKhoan == value.taiKhoan
        );
        if (index != -1) {
          state.error = {
            ...state.error,
            taiKhoan: "Tài Khoản này đã tồn tại",
          };
          isValid = false;
        }
        //Kiểm tra value
        for (let key in value) {
          if (value[key] == "") {
            state.error = { ...state.error, [key]: `* ${key} chưa điền` };
            isValid = false;
          }
        }
        //kiểm tra lỗi
        for (let key in error) {
          if (error[key] !== "") {
            isValid = false;
          }
        }
        // kiểm tra valid
        if (isValid) {
          state.danhSachQuanLy = [...state.danhSachQuanLy, state.value];
          state.value = {
            taiKhoan: "",
            matKhau: "",
            email: "",
            hoTen: "",
            soDienThoai: "",
            loaiNguoiDung: "",
          };
          Swal.fire({
            title: "Đăng ký thành công!",
            icon: "success",
            showConfirmButton: false,
          });
        } else {
          Swal.fire({
            title: "Đăng ký thất bại!",
            icon: "error",
            showConfirmButton: false,
          });
        }
        return { ...state };
      }
      break;
    case DELETE_USER:
      {
        let index = state.danhSachQuanLy.findIndex(
          (user) => user.taiKhoan == payload
        );
        state.danhSachQuanLy.splice(index, 1);
        return { ...state, danhSachQuanLy: [...state.danhSachQuanLy] };
      }
      break;
    case EDIT_USER:
      {
        return { ...state, value: payload };
      }
      break;
    case UPDATE_USER:
      {
        //validation submit
        let isValid = true;
        let { value, error } = state;
        let index = state.danhSachQuanLy.findIndex(
          (user) => user.taiKhoan == payload.taiKhoan
        );
        //Kiểm tra value
        for (let key in value) {
          if (value[key] == "") {
            state.error = { ...state.error, [key]: `* ${key} chưa điền` };
            isValid = false;
          }
        }
        //kiểm tra lỗi
        for (let key in error) {
          if (error[key] !== "") {
            isValid = false;
          }
        }
        // kiểm tra valid
        if (isValid) {
          state.danhSachQuanLy[index] = payload;
          state.danhSachQuanLy = [...state.danhSachQuanLy];
          state.value = {
            taiKhoan: "",
            matKhau: "",
            email: "",
            hoTen: "",
            soDienThoai: "",
            loaiNguoiDung: "",
          };
          Swal.fire({
            title: "Cập Nhật thành công!",
            icon: "success",
            showConfirmButton: false,
          });
        } else {
          Swal.fire({
            title: "Cập Nhật thất bại!",
            icon: "error",
            showConfirmButton: false,
          });
        }
        return { ...state };
      }
      break;

    default:
      return state;
  }
};
