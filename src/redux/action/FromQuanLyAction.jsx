import {
  CHANGE_VALUE,
  DELETE_USER,
  EDIT_USER,
  SUB_MIT,
  UPDATE_USER,
} from "../constant/FromQuanLyConstant";

export const changeValueAction = (name, value) => {
  return {
    type: CHANGE_VALUE,
    payload: {
      name,
      value,
    },
  };
};
export const handleSubmitAction = () => ({
  type: SUB_MIT,
});
export const handleDeleteUser = (taiKhoanUser) => ({
  type: DELETE_USER,
  payload: taiKhoanUser,
});
export const handleEditUserAction = (user) => ({
  type: EDIT_USER,
  payload: user,
});
export const handleUpdateUserAction = (user) => ({
  type: UPDATE_USER,
  payload: user,
});
